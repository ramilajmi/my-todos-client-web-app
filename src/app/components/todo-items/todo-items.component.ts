import { Component, OnInit} from '@angular/core';
import { TodoItem } from '../../models/todo-item.model';
import { TodoAppService } from '../../services/todo-app.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-todo-items',
  templateUrl: './todo-items.component.html',
  styleUrls: ['./todo-items.component.css']
})
export class TodoItemsComponent implements OnInit {

  todos: any;
  todoData: TodoItem = new TodoItem();
  searchAuthor = "";
  subscription: Subscription;
  displayedModal = false;

  constructor(
    public service: TodoAppService,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal) {

      //Subscribe actions from modal to refresh list
      this.subscription = this.service.onQuery().subscribe(query => {
          if (query) {
              this.refreshList();
          }
      });

    }
  
  ngOnInit(): void {
    this.refreshList();
  }

  //Get list of Todos
  refreshList(){
    this.service.getTodos()
    .subscribe({
      next: (todos: any) => {
        console.log("Todos loaded ...");
        this.todos = todos;
      },
      error: error => {
        //Handle error
        console.log(error);
      }
    });
  }
  
  //Populate Todo List
  populateForm(selectedRecord: TodoItem) {
    this.todoData = Object.assign({}, selectedRecord);
  }

  //Delete a Todo
  onDelete(id: any) {
    if (confirm("Are you sure you want to delete this record?")) {
      this.service.deleteToDoItem(id)
      .subscribe({
        next: data => {
          this.refreshList();
        },
        error: error => {
        //Handle error
          console.log(error);
        }
      });
    }
  }

  //Open update popup
  open(action: any, id?: any) {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.action = action;
    if(id != null){
      let data = this.todos.find((x: any) => x._id === id);
      modalRef.componentInstance.fromParent = data;
    }
    modalRef.result.then((result) => {
    console.log(result);
    }, (reason) => {
    });
  }
  
  ngOnDestroy() {
    //Unsubscribe
    this.subscription.unsubscribe();
  }
    
}
