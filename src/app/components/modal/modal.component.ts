import { Component, OnInit, Input } from '@angular/core';
import { TodoAppService } from '../../services/todo-app.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() fromParent: any;
  @Input() action: any;

  dialogBox = {
    title: "",
    buttonOne: "",
    buttonTwo: ""
  }

  form = {
    id: "",
    title: "",
    author: "",
    content: "",
    status: false
  }

  constructor(public activeModal: NgbActiveModal,
    public service: TodoAppService) { }

    todoForm: any;

  ngOnInit(): void {

    //init dialog box in UI
    this.initDialogBox();

  }

  //init dialog box
  initDialogBox() {
    console.log("serval : "+this.action);
    if(this.action == 0){
      if(this.fromParent != null){
        this.dialogBox.title = "Update Todo";
        this.form = this.fromParent;
        this.dialogBox.buttonTwo = "Save changes";
      }else{
        this.dialogBox.title = "Create Todo";
        this.dialogBox.buttonTwo = "Create";
      }
    }else{
      this.form = this.fromParent;
      this.dialogBox.title = "Show Todo";
      this.dialogBox.buttonTwo = "Hide";
    }
    this.dialogBox.buttonOne = "Cancel"; 
  }

  //Action to did when user click on buttonTwo
  doAction () {
    if(this.checkForm()){
      if(this.action == 0){
        if(this.form.id != ""){
          this.onEditTodo(this.form);
        }else{
          this.onCreateTodo(this.form);
        }
      }
      this.activeModal.dismiss();
    }
  }

  // Create new Todo
  onCreateTodo(data: any): void {
    this.service.postToDoItem(data)
    .subscribe({
      next: result => {
        console.log(result)
        this.service.subject.next({ value: true });
      },
      error: error => {
      //Handle error
        console.log(error);
      }
    });
  }

  //Edit/Update a Todo
  onEditTodo(data: any): void {
    this.service.putToDoItem(data._id, data)
    .subscribe({
      next: result => {
      },
      error: error => {
        // handle error
        console.log(error);
      }
    });
  }

  checkForm(): boolean{
    if(this.form.title != "" && this.form.author != "" && this.form.content != ""){
      return true;
    }else{
      return false;
    }
  }
}