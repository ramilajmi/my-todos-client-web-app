export class TodoItem {
    _id: string="";
    title: string="";
    author: String="";
    content: string="";
    status: boolean=false;
}
