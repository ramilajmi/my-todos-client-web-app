var faker = require('faker');

var database = { todos: []};

for (var i = 1; i<= 50; i++) {
  database.todos.push({
    id: i,
    title: faker.lorem.word(),
    content: faker.lorem.sentences(),
    author: faker.name.firstName(),
    status: faker.datatype.boolean()
  });
}

console.log(JSON.stringify(database));