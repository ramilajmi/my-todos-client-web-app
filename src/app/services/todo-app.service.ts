import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { TodoItem } from '../models/todo-item.model';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoAppService {

  readonly baseURL = "http://localhost:3000/api/todos";
  subject = new Subject<any>();
  list: TodoItem[]=[];

  constructor(private http: HttpClient) { }

  todoData: TodoItem = new TodoItem();

  //Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  //Create Todo
  postToDoItem(data: any): Observable<any> {
    return this.http.post(this.baseURL, data, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  //Fetch todos list
  getTodos(): Observable<TodoItem[]> {
    return this.http.get<TodoItem[]>(this.baseURL).pipe(
        catchError(this.handleError)
    );
  }

  //Get Todo with id
  getTodo(id: any): Observable<any> {
    return this.http.get(`${this.baseURL}/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  //Edit / Update Todo
  putToDoItem(id:any, data: any): Observable<any> {
    return this.http.put(`${this.baseURL}/${id}`, data, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }
 
  //Delete Todo
  deleteToDoItem(id: any): Observable<any> {
    return this.http.delete(`${this.baseURL}/${id}`, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    );
  }

  //Sending query observable
  onQuery(): Observable<any> {
    return this.subject.asObservable();
  }

  //Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    return throwError(() => 'Something bad happened; please try again later.');
  };

}
