import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }

  private modals: any[] = [];

  //Add modal
  add(modal: any) {
    this.modals.push(modal);
  }

  //Remove modal
  remove(id: string) {
    this.modals = this.modals.filter(x => x.id !== id);
  }

  //Open modal
  open(id: string) {
      let modal: any = this.modals.filter(x => x.id === id)[0];
      modal.open();
  }

  //Close modal
  close(id: string) {
      let modal: any = this.modals.filter(x => x.id === id)[0];
      modal.close();
  }
}
