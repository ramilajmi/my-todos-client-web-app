import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

import { TodoAppService } from './todo-app.service';

import { TodoItem } from '../models/todo-item.model';

describe('TodoAppService', () => {
  let httpTestingController: HttpTestingController;
  let service: TodoAppService;
  let todo: TodoItem;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TodoAppService]
    });
    service = TestBed.inject(TodoAppService);
    httpTestingController = TestBed.inject(HttpTestingController);
    todo = {
      _id: '1',
      title: 'abcd',
      author: "testerOne",
      content: "Testing part 1",
      status: false
    };
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  //Unit test for fetch todoq (GET)
  it("should return todos - GET Todos", () => {
    let result: TodoItem[];
    service.getTodos().subscribe(t => {
      result = t;
      expect(result[0]).toEqual(todo);
    });
    const req = httpTestingController.expectOne({
      method: "GET"
    });
    req.flush([todo]);  
  });
  
  //Unit test for fetch specific todo with id (GET)
  it("should return specific todo - GET Todo :id", () => {
    service.getTodo(todo._id).subscribe(result => {
      expect(result).toEqual(todo);
    });
    const req = httpTestingController.expectOne({
      method: "GET"
    });
  });
  
  //Unit test for create todo (POST)
  it("should call POST API to create a new todo - POST Todo", () => {
    service.postToDoItem(todo).subscribe();
    
    let req = httpTestingController.expectOne({ method: "POST"});
    expect(req.request.body).toEqual(todo);
  });
  
  //Unit test for Update todo (PUT)
  it("should call PUT API to update a todo", () => {
    service.putToDoItem(todo._id, todo).subscribe();
    
    let req = httpTestingController.expectOne({
      method: "PUT"
    });
    expect(req.request.body).toEqual(todo);
  });
  
});
